package Thread;

import java.util.*;
import java.util.stream.*;

import Synchronous.*;

public class PrimmThreaded {
    private static Object locker;
    private Graph graph;
    private Graph resultGraph;

    public PrimmThreaded(Graph graph) {
        this.graph = new Graph(graph);
        locker = new Object();
    }

    public Graph Result() throws InterruptedException {
        resultGraph = new Graph();
        var nodes = graph.getAllNodes();
        Map<Character, List<Vertex>> nodeMap = new HashMap<>();

        Thread nodeMapThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (Character node : nodes) {
                    nodeMap.put(node, findConnectionsForNode(node));
                }
            }
        });

        nodeMapThread.setName("nodeMapThread");
        nodeMapThread.run();

        char startNode = nodes.stream().findFirst().get();

        nodeMapThread.join();
        Vertex startVertex = findMinVertex(startNode, nodeMap);
        for (Character node : nodeMap.keySet()) {
            nodeMap.get(node).remove(startVertex);
        }

        resultGraph.getVertexList().add(startVertex);

        // start cycle
        do {
            var resultNodes = resultGraph.getAllNodes();
            Set<Vertex> minVertexList = new HashSet<>();
            List<Thread> threads = new ArrayList<>();
            for (Character node : resultNodes) {
                if (!nodeMap.containsKey(node))
                    continue;

                Thread nodeThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (locker) {
                            if (nodeMap.get(node).size() == 0) {
                                nodeMap.remove(node);
                                return;
                            }
                            minVertexList.add(findMinVertex(node, nodeMap));
                        }
                    }
                });
                nodeThread.setName("node " + node);
                threads.add(nodeThread);
            }

            threads.stream().forEach(x -> {
                x.run();
            });

            threads.stream().forEach(x -> {
                try {
                    x.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            
            if(minVertexList.size() == 0)
                break;

            var sortedVertexes = minVertexList.stream()
                    .filter(x -> !resultNodes.contains(x.getFirstNode())
                            || !resultNodes.contains(x.getSecondNode()))
                    .sorted(new WeightComparator())
                    .collect(Collectors.toList());

            if(sortedVertexes.size() == 0){
                for (Vertex vertex: minVertexList) {
                    for (Character node : nodeMap.keySet()) {
                        nodeMap.get(node).remove(vertex);
                    }
                }
                continue;
            }

            Vertex minVertex = sortedVertexes.get(0);

            for (Character node : nodeMap.keySet()) {
                nodeMap.get(node).remove(minVertex);
            }
            resultGraph.getVertexList().add(minVertex);
        } while(nodes.size() != resultGraph.getAllNodes().size());
        //end cycle
        return  resultGraph;
    }

    private List<Vertex> findConnectionsForNode(char node){
        return graph.getVertexList().stream()
            .filter(x -> x.contains(node))
            .collect(Collectors.toList());
    }

    private Vertex findMinVertex(char node, Map<Character, List<Vertex>> nodeMap){
        return nodeMap.get(node).stream()
        .min(new WeightComparator())
        .get();
    }    
}
