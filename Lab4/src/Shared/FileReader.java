package Shared;

import java.io.*;
import java.util.*;

public class FileReader {
    public static List<String> ReadFile(String name) throws FileNotFoundException{
        File f = new File(name);
        if(!f.exists())
            throw new FileNotFoundException();
        
        Scanner s = new Scanner(f);
        List<String> list = new ArrayList<String>();
        while (s.hasNextLine()) {
            list.add(s.nextLine());
        }
        s.close();
        return list;
    }
}
