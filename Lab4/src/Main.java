import java.io.FileNotFoundException;
import java.util.*;

import Executor.PrimmExecutor;
import Shared.*;
import Synchronous.*;
import Thread.PrimmThreaded;
import UI.MainWindow;

public class Main {
    public static void main(String[] args) {
        MainWindow mw = new MainWindow();
        mw.setVisible(true);
    }
}
