package Synchronous;

import java.util.*;
import java.util.stream.Collectors;

public class PrimmAlgorithm {
    private Graph graph;
    private Graph resultGraph;

    public PrimmAlgorithm(Graph graph){
        this.graph = new Graph(graph);
    }

    public Graph Result(){
        resultGraph = new Graph();
        var nodes = graph.getAllNodes();
        Map<Character,List<Vertex>> nodeMap = new HashMap<>();
        for (Character node : nodes) {
            nodeMap.put(node, findConnectionsForNode(node));
        }

        char startNode = nodes.stream()
            .findFirst()
            .get();
        
        Vertex startVertex = findMinVertex(startNode, nodeMap);
        for (Character node : nodeMap.keySet()) {
            nodeMap.get(node).remove(startVertex);
        }

        resultGraph.getVertexList().add(startVertex);
        
        //start cycle
        do {
            var resultNodes = resultGraph.getAllNodes();
            Set<Vertex> minVertexList= new HashSet<>();
            for (Character node : resultNodes) {
                if(!nodeMap.containsKey(node))
                    continue;
                
                if(nodeMap.get(node).size() == 0) {
                    nodeMap.remove(node);
                    continue;
                }
                minVertexList.add(findMinVertex(node, nodeMap));
            }

            if(minVertexList.size() == 0)
                break;

            var sortedVertexes = minVertexList.stream()
                    .filter(x -> !resultNodes.contains(x.getFirstNode())
                            || !resultNodes.contains(x.getSecondNode()))
                    .sorted(new WeightComparator())
                    .collect(Collectors.toList());

            if(sortedVertexes.size() == 0){
                for (Vertex vertex: minVertexList) {
                    for (Character node : nodeMap.keySet()) {
                        nodeMap.get(node).remove(vertex);
                    }
                }
                continue;
            }

            Vertex minVertex = sortedVertexes.get(0);

            for (Character node : nodeMap.keySet()) {
                nodeMap.get(node).remove(minVertex);
            }
            resultGraph.getVertexList().add(minVertex);
        } while(nodes.size() != resultGraph.getAllNodes().size());
        //end cycle
        return  resultGraph;
    }

    private List<Vertex> findConnectionsForNode(char node){
        return graph.getVertexList().stream()
            .filter(x -> x.contains(node))
            .collect(Collectors.toList());
    }

    private Vertex findMinVertex(char node, Map<Character, List<Vertex>> nodeMap){
        return nodeMap.get(node).stream()
        .min(new WeightComparator())
        .get();
    }
}
