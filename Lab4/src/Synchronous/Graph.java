package Synchronous;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Graph {
    private List<Vertex> vertexList;

    public Graph() {
        vertexList = new ArrayList<>();
    }

    public Graph(Graph graph) {
        this.vertexList = new ArrayList<>(graph.vertexList);
    }

    public Graph(List<Vertex> vertexList){
        this.vertexList = vertexList;
    }

    public List<Vertex> getVertexList() {
        return vertexList;
    }

    public boolean isEmpty(){
        return vertexList.isEmpty();
    }

    public List<Vertex> getNearestVertexes(Vertex neighbour){
        return vertexList.stream()
                .filter(x-> x.contains(neighbour.getFirstNode()) ||
                        x.contains(neighbour.getSecondNode()))
                .sorted(new WeightComparator())
                .collect(Collectors.toList());
    }

    public Set<Character> getAllNodes(){
        Set<Character> nodes = new HashSet<>();
        for (Vertex vertex : vertexList) {
            nodes.add(vertex.getFirstNode());
            nodes.add(vertex.getSecondNode());
        }
        return nodes;
    }
}
