package Synchronous;
public class Vertex {
    private char firstNode;
    private char secondNode;
    private int weight;

    public Vertex(){}

    public Vertex(char first, char second, int weight){
        firstNode = first;
        secondNode = second;
        this.weight = weight;
    }

    public char getFirstNode() {
        return firstNode;
    }

    public  void setFirstNode(char node){
        firstNode = node;
    }

    public char getSecondNode(){
        return  secondNode;
    }

    public void setSecondNode(char node){
        secondNode = node;
    }

    public int getWeight(){
        return weight;
    }

    public void setWeight(int newWeight){
        weight = newWeight;
    }

    @Override
    public String toString() {
        return new String (new char[] {firstNode, secondNode}) 
            + String.valueOf(weight);
    }

    public boolean contains(char node){
        return node == firstNode || node == secondNode;
    }

    public boolean isConnectedWith(Vertex vertex){
        return this.contains(vertex.firstNode) ||
                this.contains(vertex.secondNode);
    }

    public  boolean hasCommonNode(Vertex v){
        return this.contains(v.firstNode) || this.contains(v.secondNode);
    }

    public  char getCommonNode(Vertex v){
        if (v.contains(firstNode))
            return firstNode;

        if(v.contains(secondNode))
            return  secondNode;

        return '\0';
    }

    public char getAnotherNode(char node){
        if(node == firstNode)
            return secondNode;
        
        return firstNode;
    }

    @Override
    public boolean equals(Object o){
        if(o.getClass() != this.getClass())
            return false;
        
            Vertex ver = (Vertex) o;
        if(ver.contains(firstNode) && ver.contains(secondNode) && ver.weight == weight)
            return true;
        
        return false;
    }
}
