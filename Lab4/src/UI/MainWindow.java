package UI;

import java.awt.*;
import java.awt.event.*;
import java.beans.IntrospectionException;
import java.io.FileNotFoundException;
import java.net.ConnectException;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import Shared.*;
import Synchronous.*;
import Thread.*;
import Executor.*;
import java.util.*;
import java.util.List;

public class MainWindow extends JFrame {
    private JButton runButton;
    private JTextArea textArea;

    public MainWindow() {
        super("Monitor");
        setBounds(200, 200, 500, 700);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = getContentPane();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        InitializeComponents(container);
    }

    private void InitializeComponents(Container container) {
        runButton = new JButton();
        runButton.setText("Run threads");
        runButton.addActionListener(new RunButtonEventListener());
        runButton.setMaximumSize(new Dimension(100, 20));

        textArea = new JTextArea();
        textArea.setLineWrap(true);
        JScrollPane sp = new JScrollPane(textArea);
        container.add(sp);
        container.add(runButton);
    }

    private class RunButtonEventListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            new Monitor(textArea).start();
        } 
    }
}
