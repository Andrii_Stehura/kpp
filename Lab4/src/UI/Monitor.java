package UI;

import java.awt.*;
import java.awt.event.*;
import java.beans.IntrospectionException;
import java.io.FileNotFoundException;
import java.net.ConnectException;


import javax.swing.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import Shared.*;
import Synchronous.*;
import Thread.*;
import Executor.*;

import java.util.*;
import java.util.List;
import  java.util.Timer;

public class Monitor extends Thread {
    private JTextArea textArea;

    public Monitor(JTextArea ta) {
        textArea = ta;
    }

    @Override
    public void start() {
        List<String> strings;
        try {
            strings = FileReader.ReadFile(
                    "D:\\University\\student\\3 kurs\\semester 5\\Cross-platform programming\\VertexList.txt");
        } catch (FileNotFoundException nfne) {
            System.out.println(nfne.getMessage());
            return;
        }

        List<Vertex> vertexs = new ArrayList<>();
        for (String row : strings) {
            var tokens = row.split(" ");
            char firstNode = tokens[0].charAt(0);
            for (int i = 1; i < tokens.length; ++i) {
                char secondNode = tokens[i].charAt(0);
                int weight = Integer.parseInt(tokens[i].substring(1, tokens[i].length()));
                Vertex newVertex = new Vertex(firstNode, secondNode, weight);
                boolean isAlreadyAdded = vertexs.stream().anyMatch(x -> x.equals(newVertex));
                if (!isAlreadyAdded)
                    vertexs.add(newVertex);
            }
        }

        Graph g = new Graph(vertexs);
        Thread syncThread = new Thread(new Runnable() {
            @Override
            public void run() {
                PrimmAlgorithm pa = new PrimmAlgorithm(g);
                Graph resultGraph = pa.Result();
                System.out.println("synchronous:");
                for (Vertex vertex : resultGraph.getVertexList()) {
                    System.out.println(vertex);
                }
            }
        });
        syncThread.setName("Synchronous thread");

        Thread threadedThread = new Thread(new Runnable() {
            @Override
            public void run() {
                PrimmThreaded pt = new PrimmThreaded(g);
                Graph resultGraph;
                try {
                    resultGraph = pt.Result();
                    System.out.println("threaded:");
                    for (Vertex vertex : resultGraph.getVertexList()) {
                        System.out.println(vertex);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        });
        threadedThread.setName("Threads");

        Thread executorThread = new Thread(new Runnable() {
            @Override
            public void run() {
                PrimmExecutor pe = new PrimmExecutor(g);
                Graph resultGraph;
                try {
                    resultGraph = pe.Result();
                    System.out.println("exe:");
                    for (Vertex vertex : resultGraph.getVertexList()) {
                        System.out.println(vertex);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        executorThread.setName("Executor");
        try {
            Thread.currentThread().sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        writeAboutThreadLineOnTextArea(textArea, syncThread);
        writeAboutThreadLineOnTextArea(textArea, threadedThread);
        writeAboutThreadLineOnTextArea(textArea, executorThread);
        textArea.getParent().invalidate();
        syncThread.start();
        threadedThread.start();
        executorThread.start();

        while (syncThread.isAlive()) {
            writeAboutThreadLineOnTextArea(textArea, syncThread);
            writeAboutThreadLineOnTextArea(textArea, threadedThread);
            writeAboutThreadLineOnTextArea(textArea, executorThread);
            try {
                syncThread.sleep(500);
                threadedThread.sleep(500);
                executorThread.sleep(500);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            writeAboutThreadLineOnTextArea(textArea, syncThread);
            writeAboutThreadLineOnTextArea(textArea, threadedThread);
            writeAboutThreadLineOnTextArea(textArea, executorThread);
        }
    }

    private void writeAboutThreadLineOnTextArea(JTextArea ta, Thread t) {
        ta.append("Name: " + t.getName() + " State: "
                + t.getState().toString() + " Priority: "
                + String.valueOf(t.getPriority()) + " Is alive: "
                + String.valueOf(t.isAlive()) + '\n');
    }
}

