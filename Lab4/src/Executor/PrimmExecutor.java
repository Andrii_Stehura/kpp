package Executor;

import java.util.*;
import java.util.stream.*;
import java.util.concurrent.*;

import Synchronous.*;

public class PrimmExecutor {
    private static Object locker;
    private Graph graph;
    private Graph resultGraph;
    private ExecutorService exeService;

    public PrimmExecutor(Graph graph) {
        this.graph = new Graph(graph);
        locker = new Object();
        exeService = Executors.newFixedThreadPool(8);
    }

    public Graph Result() throws InterruptedException, ExecutionException {
        resultGraph = new Graph();
        var nodes = graph.getAllNodes();
        Map<Character, List<Vertex>> nodeMap = new HashMap<>();

        var nodeMapFilled = exeService.submit(new Runnable() {
            @Override
            public void run() {
                for (Character node : nodes) {
                    nodeMap.put(node, findConnectionsForNode(node));
                }
            }
        });
        char startNode = nodes.stream().findFirst().get();

        nodeMapFilled.get();

        Vertex startVertex = findMinVertex(startNode, nodeMap);
        for (Character node : nodeMap.keySet()) {
            nodeMap.get(node).remove(startVertex);
        }

        resultGraph.getVertexList().add(startVertex);

        // start cycle
        do {
            var resultNodes = resultGraph.getAllNodes();
            Set<Vertex> minVertexList = new HashSet<>();
            List<Future<?>> threads = new ArrayList<>();
            for (Character node : resultNodes) {
                if (!nodeMap.containsKey(node))
                    continue;

                threads.add(exeService.submit(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (locker) {
                            if (nodeMap.get(node).size() == 0) {
                                nodeMap.remove(node);
                                return;
                            }
                            minVertexList.add(findMinVertex(node, nodeMap));
                        }
                    }
                }));
            }

            for (Future<?> future : threads) {
                future.get();
            }
            
            if(minVertexList.size() == 0)
                break;

            var sortedVertexes = minVertexList.stream()
                    .filter(x -> !resultNodes.contains(x.getFirstNode())
                            || !resultNodes.contains(x.getSecondNode()))
                    .sorted(new WeightComparator())
                    .collect(Collectors.toList());

            if(sortedVertexes.size() == 0){
                for (Vertex vertex: minVertexList) {
                    for (Character node : nodeMap.keySet()) {
                        nodeMap.get(node).remove(vertex);
                    }
                }
                continue;
            }

            Vertex minVertex = sortedVertexes.get(0);

            for (Character node : nodeMap.keySet()) {
                nodeMap.get(node).remove(minVertex);
            }
            resultGraph.getVertexList().add(minVertex);
        } while(nodes.size() != resultGraph.getAllNodes().size());
        //end cycle

        exeService.shutdown();
        return  resultGraph;
    }

    private List<Vertex> findConnectionsForNode(char node){
        return graph.getVertexList().stream()
            .filter(x -> x.contains(node))
            .collect(Collectors.toList());
    }

    private Vertex findMinVertex(char node, Map<Character, List<Vertex>> nodeMap){
        return nodeMap.get(node).stream()
        .min(new WeightComparator())
        .get();
    }    
}
