import java.io.*;
import java.util.*;

public class FileParser {
    public static String ParseFile(String filename) throws FileNotFoundException{
        List<String> strings;
        try{
            strings = ReadFile(filename);
        } catch (FileNotFoundException fne){
            throw fne;
        }
        return StickTogether(strings);
    }

    private static List<String> ReadFile(String name) throws FileNotFoundException{
        File f = new File(name);
        if(!f.exists())
            throw new FileNotFoundException();
        
        Scanner s = new Scanner(f);
        List<String> list = new ArrayList<String>();
        while (s.hasNextLine()) {
            list.add(s.nextLine());
        }
        s.close();
        return list;
    }

    private static String StickTogether(List<String> list){
        StringBuilder sb = new StringBuilder();
        for (String string : list) {
            sb.append(string);
        }
        return sb.toString();
    }
}
