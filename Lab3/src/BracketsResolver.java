import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BracketsResolver{
    private String text;
    
    public BracketsResolver(String text) {
        this.text = text;
    }

    public String getTextInBrackets(){
        List<BracketPair> bracketPairs = getBracketPairs();
        var splited = splitByGroups(bracketPairs);
        StringBuilder sb = new StringBuilder();
        for (var group : splited) {
            sb.append(text.subSequence(group.get(0).getLeftIndex(),
                group.get(0).getRightIndex() + 1));
        }
        return sb.toString();
    }

    public String replaceNumbersInBrackets(){
        List<BracketPair> bracketPairs = getBracketPairs();
        var copyStr = text.toCharArray();

        for (BracketPair bracketPair : bracketPairs) {
            for(int i = bracketPair.getLeftIndex() + 1; i < bracketPair.getRightIndex(); ++i){
                if(Character.isDigit(copyStr[i]))
                    copyStr[i] = '#';
            }
        }
        return new String(copyStr);
    }

    public  String getTextInDeepestBrackets(){
        List<BracketPair> bracketPairs = getBracketPairs();
        List<BracketPair> deepestBracketsList = getDeepestBrackets(bracketPairs);
        StringBuilder sb = new StringBuilder();
        for (BracketPair bracketPair : deepestBracketsList) {
            sb.append(text.subSequence(bracketPair.getLeftIndex(),
                bracketPair.getRightIndex() + 1));
        }
        return sb.toString();
    }

    public  String deleteTextInDeepestBrackets(){
        List<BracketPair> bracketPairs = getBracketPairs();
        List<BracketPair> deepestBracketsList = getDeepestBrackets(bracketPairs);
        String copyStr = text;
        for (BracketPair bracketPair : deepestBracketsList) {
            copyStr = copyStr.replace(text.subSequence(bracketPair.getLeftIndex() + 1,
                bracketPair.getRightIndex()), "");
        }
        return copyStr;
    }

    private List<BracketPair> getDeepestBrackets(List<BracketPair> bpList){
        bpList.sort((el1, el2)-> {
            if(el1.getLeftIndex() > el2.getLeftIndex())
                return 1;
            
            if(el1.getLeftIndex() == el2.getLeftIndex())
                return 0;
            
            return -1;
        });
        var splited = splitByGroups(bpList);
        List<BracketPair> deepestBracketsList = new ArrayList<>();
        for (List<BracketPair> list : splited) {
            deepestBracketsList.add(recoursiveSearch(list, list.get(0)));
        }
        return deepestBracketsList;
    }

    private List<List<BracketPair>> splitByGroups(List<BracketPair> bpList){
        List<BracketPair> bpCopy = new ArrayList<>(bpList);
        bpCopy.sort((x,y)->Integer.compare(x.getLeftIndex(), y.getLeftIndex()));
        List<List<BracketPair>> splited = new ArrayList<>();

        while(bpCopy.size() > 0) {
            List<BracketPair> group = new ArrayList<>();
            BracketPair bp = bpCopy.get(0);
            group.add(bp);
            bpCopy.remove(0);
            while(bp.getRightIndex() > bpCopy.get(0).getLeftIndex()){
                group.add(bpCopy.get(0));
                bpCopy.remove(0);
                if(bpCopy.size() == 0)
                    break;
            }
            splited.add(group);
        }
        return splited;
    }

    private BracketPair recoursiveSearch(List<BracketPair> bpList, BracketPair outer){
        BracketPair inner = null;
        for (BracketPair bracketPair : bpList) {
            if(bracketPair.getLeftIndex() > outer.getLeftIndex() &&
                bracketPair.getRightIndex() < outer.getRightIndex()){
                inner = bracketPair;
                break;
            }
        }
        if(inner != null)
            return recoursiveSearch(bpList, inner);

        return outer;
    }

    private List<BracketPair> getBracketPairs(){
        Map<Integer, Character> map = new HashMap<Integer, Character>();
        for (int i = 0; i < text.length(); ++i) {
            char ch = text.charAt(i);
            if(ch =='(' || ch == ')')
                map.put(i, ch);
        }

        List<BracketPair> bpList = new ArrayList<>();
        while(true){
            boolean addedPair = false;
            int prev = -1;
            int curr = -1;
            List<Integer> keyList = new ArrayList<>(map.keySet());
            keyList.sort((x, y)->Integer.compare(x, y));
            for (int key : keyList){
                if(map.get(key) == '('){
                    prev = key;
                    continue;
                }
                if(prev == -1)
                    continue;
                curr = key;
                bpList.add(new BracketPair(prev, key));
                addedPair = true;
                break;
            }
            if(!addedPair)
                break;
            
            map.remove(prev);
            map.remove(curr);
        }
        return bpList;
    }
}