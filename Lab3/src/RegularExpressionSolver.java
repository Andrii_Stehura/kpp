import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressionSolver {
    private String text;

    public RegularExpressionSolver(String text){
        this.text = text;
    }

    public String deleteArticlesFromText(){
        Matcher matcher = Pattern.compile("\\b(the|an|are|on|or|out|in|a)\\b",
                Pattern.CASE_INSENSITIVE).matcher(text);
        return matcher.replaceAll("");
    }
}
