import org.junit.jupiter.api.Test;
import org.junit.platform.commons.annotation.Testable;
import java.io.FileNotFoundException;
import static org.junit.jupiter.api.Assertions.*;


@Testable
public class RegularExpressionSolverTest {

    @Test
    public void TestRegExp(){
        final String TEXT_PATH = "D:\\University\\student\\3 kurs\\semester 5\\Cross-platform programming\\Lab3\\RegexLab\\for_regexp.txt";
        RegularExpressionSolver res = null;
        try {
            res = new RegularExpressionSolver(FileParser.ParseFile(TEXT_PATH));
        } catch (FileNotFoundException fne){
            assertFalse(true);
        }
        assertEquals("London is  capital of Great Britain. Have you ever been to London?" +
                " Have you ever wondered to have  english breakfast at 9 o'clock " +
                "  top of Big Ban? There   lot of interesting things  this city." +
                " I don't think that London will run  of tea.", res.deleteArticlesFromText());
    }
}