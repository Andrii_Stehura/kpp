import java.io.*;
import java.util.*;

public class App {
    final static String UA_PATH = "D:\\University\\student\\3 kurs\\semester 5\\Cross-platform programming\\Lab3\\RegexLab\\text_ua.txt";
    final static String EN_PATH = "D:\\University\\student\\3 kurs\\semester 5\\Cross-platform programming\\Lab3\\RegexLab\\text_en.txt";
    public static void main(String[] args) throws Exception {
        Task1UA();
        Task1EN();
    }

    private static void Task1UA(){
        BracketsResolver br;
        try{
             br = new BracketsResolver(FileParser.ParseFile(UA_PATH));
        }
        catch(FileNotFoundException fne){
            System.out.println(fne);
            return;
        }
        System.out.println("Текст в дужках:");
        System.out.println(br.getTextInBrackets());
        System.out.println("Текст в найбільш вкладених дужках:");
        System.out.println(br.getTextInDeepestBrackets());
        System.out.println("Текст, з видаленим текстом в найбільш вкладених в дужках:");
        System.out.println(br.deleteTextInDeepestBrackets());
        System.out.println("Текст із заміненими цифрами в дужках:");
        System.out.println(br.replaceNumbersInBrackets());
    }

    private static void Task1EN(){
        BracketsResolver br;
        try{
             br = new BracketsResolver(FileParser.ParseFile(EN_PATH));
        }
        catch(FileNotFoundException fne){
            System.out.println(fne);
            return;
        }
        System.out.println("Текст в дужках:");
        System.out.println(br.getTextInBrackets());
        System.out.println("Текст в найбільш вкладених дужках:");
        System.out.println(br.getTextInDeepestBrackets());
        System.out.println("Текст, з видаленим текстом в найбільш вкладених в дужках:");
        System.out.println(br.deleteTextInDeepestBrackets());
        System.out.println("Текст із заміненими цифрами в дужках:");
        System.out.println(br.replaceNumbersInBrackets());
    }
}

