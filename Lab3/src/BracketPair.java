public class BracketPair {
    private int leftIndex;
    private int rightIndex;
    
    public BracketPair(int left, int right) {
        leftIndex = left;
        rightIndex = right;
    }
    
    public int getLeftIndex() {
        return leftIndex;
    }

    public void setLeftIndex(int leftIndex) {
        this.leftIndex = leftIndex;
    }

    public int getRightIndex() {
        return rightIndex;
    }

    public void setRightIndex(int rightIndex) {
        this.rightIndex = rightIndex;
    }
}
