package Interfaces;

public interface IAreaCalculateable {
    public float getArea();
}