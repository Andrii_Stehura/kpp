package models;

import Interfaces.*;

public class Floor implements IAreaCalculateable{
    private Room[] _rooms;
    
    public Floor(Room[] rooms) {
        _rooms = rooms;
    }

    public void setRooms(Room[] _rooms) {
        this._rooms = _rooms;
    }

    public Room[] getRooms() {
        return _rooms;
    }

    @Override
    public float getArea() {
        float total = 0;
        for (Room room : _rooms) {
            total += room.getArea();
        }
        return total;
    }
}
