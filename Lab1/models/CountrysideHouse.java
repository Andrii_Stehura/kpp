package models;

import Enumerations.*;

public class CountrysideHouse extends House{
    private float _outsideArea;

    public CountrysideHouse(Floor[] floors, Address address, String description,
                Tags[] tags, float outsiadeArea) {
        super(floors, address, description, tags);
        _outsideArea = outsiadeArea;
        for (Tags tag : tags) {
            if(tag == Tags.COUNTRYSIDE)
                return;
        }
        Tags[] newTags = new Tags[tags.length + 1];
        for(int i = 0; i < tags.length; ++i)
            newTags[i] = tags[i];
        newTags[tags.length] = Tags.COUNTRYSIDE;
        _tags = newTags;
    }

    public void setOutsideArea(float outsideArea) {
        this._outsideArea = outsideArea;
    }

    public float getOutsideArea() {
        return _outsideArea;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append("\nOutside area: ").append(_outsideArea);
        return sb.toString();
    }
}