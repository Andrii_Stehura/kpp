package models;

import Enumerations.*;

public class Flat extends House {
    private int _floorNumber;
    private int _flatNumber;

    public Flat(Floor[] floors, Address address, String description,
                Tags[] tags, int floorNumber, int flatNumber) {
        super(floors, address, description, tags);
        _flatNumber = flatNumber;
        _floorNumber = floorNumber;
        for (Tags tag : tags) {
            if(tag == Tags.FLAT)
                return;
        }
        Tags[] newTags = new Tags[tags.length + 1];
        for(int i = 0; i < tags.length; ++i)
            newTags[i] = tags[i];
        newTags[tags.length] = Tags.FLAT;
        _tags = newTags;
    }

    public void setFloorNumber(int floorNumber) {
        this._floorNumber = floorNumber;
    }

    public int getFloorNumber() {
        return _floorNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this._flatNumber = flatNumber;
    }

    public int getFlatNumber() {
        return _flatNumber;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append("\nFloor: ").append(_floorNumber).append("\n");
        sb.append("Flat: ").append(_flatNumber);
        return sb.toString();
    }
}
