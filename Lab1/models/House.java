package models;

import Interfaces.*;
import Enumerations.*;

public class House implements IAreaCalculateable{
    protected Floor[] _floors;
    protected Address _address;
    protected String _description;
    protected Tags[] _tags;

    public House(Floor[] floors, Address address, String description, Tags[] tags) {
        _floors = floors;
        _address = address;
        _description = description;
        _tags = tags;
    }

    public void setTags(Tags[] tags) {
        this._tags = tags;
    }

    public Tags[] getTags() {
        return _tags;
    }

    public void setDescription(String _description) {
        this._description = _description;
    }

    public String getDescription() {
        return _description;
    }

    public void setAddress(Address address) {
        this._address = address;
    }

    public Address getAddress() {
        return _address;
    }

    public void setFloors(Floor[] _floors) {
        this._floors = _floors;
    }

    public Floor[] getFloors() {
        return _floors;
    }

    @Override
    public float getArea() {
        float total = 0;
        for (Floor floor : _floors) {
            total += floor.getArea();
        }
        return total;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Address: ").append(_address.toString()).append('\n');
        sb.append("Floors: ").append(_floors.length).append('\n');
        sb.append("Rooms: ");
        int rooms = 0;
        for (Floor floor : _floors) {
            rooms += floor.getRooms().length;
        }
        sb.append(rooms).append('\n');
        sb.append("Area: ").append(getArea()).append('\n');
        sb.append("Description: ").append(_description).append('\n');
        sb.append("Tags: ");
        for (Tags tag : _tags) {
            sb.append(tag).append(' ');
        }

        return sb.toString();
    }
}
