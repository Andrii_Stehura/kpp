package models;

import Interfaces.*;

public class Room implements IAreaCalculateable{
    private float _length;
    private float _width;
    
    public Room(float length, float width) {
        this._length = length;
        this._width = width;
    }

    public void setLength(float value){
        _length = value;
    }

    public float getLength(){
        return _length;
    }

    public void setWidth(float value){
        _width = value;
    }

    public float getWidth(){
        return _width;
    }

    @Override
    public float getArea() {
        return _length * _width;
    }
}
