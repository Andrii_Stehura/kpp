package models;

public class Address {
    private String _houseNumber;
    private String _street;
    private String _city;
    private String _state;
    private String _country;

    public void setCountry(String country) {
        this._country = country;
    }

    public String getCountry() {
        return _country;
    }
    
    public void setState(String state) {
        this._state = state;
    }

    public String getState() {
        return _state;
    }
    
    public void setCity(String city) {
        this._city = city;
    }

    public String getCity() {
        return _city;
    }

    public void setStreet(String street) {
        this._street = street;
    }

    public String getStreet() {
        return _street;
    }

    public void setHouseNumber(String houseNumber) {
        this._houseNumber = houseNumber;
    }

    public String getHouseNumber() {
        return _houseNumber;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        if(_country != null)
            sb.append(_country).append(',');
        if(_state != null)
            sb.append(_state).append(',');
        if(_city != null)
            sb.append(_city).append(',');
        if(_street != null)
            sb.append(_street).append(',');
        if(_houseNumber != null)
            sb.append(_houseNumber);
        return sb.toString();
    }
}
