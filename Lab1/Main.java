import models.*;
import java.io.*;
import java.util.*;
import Enumerations.*;
import Managers.*;


public class Main{
    public static void main(String[] args) {
        HouseManager hm = new HouseManager();
        for(int i = 0; i < 5; ++i){
            hm.getHouses().add(new House(
                getRandomFloors(),
                getRandomAddress(), 
                "Some description", 
                getRandomTags())
            );
        }

        Random r = new Random();
        for(int i = 0; i < 5; ++i){
            hm.getHouses().add(new Flat(
                getRandomFloors(),
                getRandomAddress(), 
                "Some description", 
                getRandomTags(), r.nextInt(20) + 1, r.nextInt(80) + 1)
            );
        }

        for(int i = 0; i < 5; ++i){
            hm.getHouses().add(new CountrysideHouse(
                getRandomFloors(),
                getRandomAddress(), 
                "Some description", 
                getRandomTags(), (r.nextFloat() + 1) * 40)
            );
        }

        printList(hm.getHouses());
        
        while(true){
            System.out.println("1 - sort by tags ascending");
            System.out.println("2 - sort by tags descending");
            System.out.println("3 - sort by area ascending");
            System.out.println("4 - sort by area descending");
            System.out.println("Else - exit");
            int variant;
            Console console = System.console();
            try{
                variant = Integer.parseInt(console.readLine());
            }
            catch (NumberFormatException e){
                variant = 0;
            }

            List<House> list = null;
            boolean flag = false;
            switch(variant){
                case 1:{
                    list = hm.sortByTags(true);
                    break;
                }
                case 2:{
                    list = hm.sortByTags(false);
                    break;
                }
                case 3:{
                    list = hm.sortByArea(true);
                    break;
                }
                case 4:{
                    list = hm.sortByArea(false);
                    break;
                }
                default: 
                    flag = true;
            }
            if(flag)
                break;
            printList(list);
        }
        
    }

    private static Floor[] getRandomFloors(){
        Random r = new Random();
        int floorsNum = r.nextInt(2);
        ++floorsNum;
        Floor[] floors = new Floor[floorsNum];
        for(int i = 0; i < floorsNum; ++i)
            floors[i] = new Floor(getRandomRooms());
        return floors;
    }

    private static Room[] getRandomRooms(){
        Random r = new Random();
        int roomsNum = r.nextInt(3);
        ++roomsNum;
        Room[] rooms = new Room[roomsNum];
        for(int i = 0; i < roomsNum; ++i)
            rooms[i] = new Room((r.nextFloat() + 1) * 4, (r.nextFloat() + 1) * 4);
        return rooms;
    }

    private static Address getRandomAddress(){
        Address a = new Address();
        a.setCountry("Україна");
        Random r = new Random();
        int index = r.nextInt(5);
        String[] states = {"Львівська обл.", "Київська обл.", "Запорізька обл.", 
            "Рівненська обл.", "Закарпатська обл."};
        a.setState(states[index]);
        String[] cities = {"м. Львів", "м. Київ", "м. Запоріжжя",
            "м. Рівне", "м. Ужгород"};
        a.setCity(cities[index]);
        String[] streets = {"вул. Б. Хмельницького", "вул. С. Бандери", 
            " вул. І. Франка", "вул. Л. Українки", "вул. М. Коцюбинського"};
        a.setStreet(streets[index]);
        a.setHouseNumber(Integer.toString(r.nextInt(100)));
        return a;
    }

    private static Tags[] getRandomTags(){
        Random r = new Random();
        int tagNums = r.nextInt(5);
        Tags[] tags = new Tags[tagNums];
        for(int i = 0; i < tagNums; ++i)
            tags[i] = Tags.values()[i];
        return tags;
    }

    private static void printList(List<House> list){
        if(list == null)
            return;

        for (House h : list) {
            System.out.println(h);
            System.out.println();
        }
    }
} 