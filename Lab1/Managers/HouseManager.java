package Managers;

import java.util.*;
import models.*;

public class HouseManager {
    private List<House> _houses;

    public HouseManager() {
        _houses = new ArrayList<House>();
    }

    public void setHouses(List<House> _houses) {
        this._houses = _houses;
    }

    public List<House> getHouses() {
        return _houses;
    }

    public List<House> sortByTags(boolean ascending)
    {
        Comparator<House> comparator = (ascending) ? new TagSortAscending() : new TagSortDescending();
        List<House> copy = new ArrayList<House>(_houses);
        Collections.sort(copy, comparator);
        return copy;
    }

    public List<House> sortByArea(boolean ascending){
        Comparator<House> comparator;
        if(ascending){
            comparator = new Comparator<House>(){
                @Override
                public int compare(House o1, House o2) {
                    return (o1.getArea() > o2.getArea()) ? 0 : -1;
                }
            };
        } else {
            comparator = (x,y)-> (x.getArea() < y.getArea()) ? 0 : -1;
        }
        List<House> copy = new ArrayList<House>(_houses);
        Collections.sort(copy, comparator);
        return copy;   
    }

    public static class TagSortAscending implements Comparator<House>{
        @Override
        public int compare(House o1, House o2) {
            return (o1.getTags().length > o2.getTags().length) ? 0 : -1;
        }
    }

    public class TagSortDescending implements Comparator<House>{
        @Override
        public int compare(House o1, House o2) {
            return (o1.getTags().length < o2.getTags().length) ? 0 : -1;
        }
    }


}
