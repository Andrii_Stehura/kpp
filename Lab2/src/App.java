import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class App {
    public static void main(String[] args) throws Exception {
        while(true)
        {
            System.out.println("Welcome! Choose your destiny (enter 1, 2 or 3 and other character to exit):");
            String str = System.console().readLine();
            switch(str)
            {
                case "1":
                {
                    FirstTask();
                    break;
                }
                case "2":
                {
                    SecondTask();
                    break;
                }
                case "3":
                {
                    ThirdTask();
                    break;
                }
                default:
                    System.exit(0); 
            }
        }
    }

    private static void FirstTask(){
        List<String> strings;
        try{
            strings = ReadFile("D:\\University\\student\\3 kurs\\semester 5\\Cross-platform programming\\List.txt");
        } catch (FileNotFoundException fne){
            System.out.println("Input file not found.");
            return;
        }
        List<Film> films = ParseToFilms(strings);
        Map<String, List<Film>> map = new HashMap<String, List<Film>>();
        Set<String> producers = new HashSet<String>();
        for (Film f : films) {
            producers.addAll(f.getProducers());
        }

        for (String producer : producers) {
            List<Film> producersFilms = new ArrayList<Film>();
            for (Film f : films) {
                if(f.getProducers().contains(producer))
                    producersFilms.add(f);
            }
            map.put(producer, producersFilms);
        }

        for (String producer : map.keySet()) {
            System.out.print("Producer: " + producer + ", films: ");
            for (Film f : map.get(producer)) {
                System.out.print(f.getName() + " ");
            }
            System.out.println();
        }
    }
    
    private static void SecondTask(){
        List<String> strings;
        try{
            strings = ReadFile("D:\\University\\student\\3 kurs\\semester 5\\Cross-platform programming\\List.txt");
        } catch (FileNotFoundException fne){
            System.out.println("Input file not found.");
            return;
        }
        List<Film> films = ParseToFilms(strings);
        Map<String, Set<String>> map = new HashMap<String, Set<String>>();
        Set<String> years = new HashSet<String>();
        for (Film film : films) {
            years.add(film.getYear());
        }

        for (String year : years) {
            Set<String> names = new HashSet<String>();
            for(Film f : films){
                if(Integer.parseInt(f.getYear()) == Integer.parseInt(year))
                    names.add(f.getName());
            }
            map.put(year, names);
        }

        for (String y : years) {
            System.out.print(y);
            for (String name : map.get(y)) {
                System.out.print(" " + name);
            }    
            System.out.println();
        }
    }

    private static void ThirdTask(){
        List<String> strings;
        try{
            strings = ReadFile("D:\\University\\student\\3 kurs\\semester 5\\Cross-platform programming\\List.txt");
        } catch (FileNotFoundException fne){
            System.out.println("Input file not found.");
            return;
        }
        try{
            strings.addAll(ReadFile("D:\\University\\student\\3 kurs\\semester 5\\Cross-platform programming\\List2.txt"));
        } catch (FileNotFoundException fne){
            System.out.println("Input file not found.");
            return;
        }

        List<Film> films = ParseToFilms(strings);
        for (Film film : films) {
            System.out.print(film.getName() + " " + film.getYear());
            System.out.print(", Director: " + film.getDirector() + ", Producers: ");
            for (String s : film.getProducers()) {
                System.out.print(" " + s);
            }
            System.out.println();
        }
    }

    private static List<String> ReadFile(String name) throws FileNotFoundException{
        File f = new File(name);
        if(!f.exists())
            throw new FileNotFoundException();
        
        Scanner s = new Scanner(f);
        List<String> list = new ArrayList<String>();
        while (s.hasNextLine()) {
            list.add(s.nextLine());
        }
        s.close();
        return list;
    }

    private static List<Film> ParseToFilms(List<String> strings){
        List<Film> films = new ArrayList<Film>();
        String[] tokens;
        for (String str : strings) {
            tokens = str.split(",");
            String name = tokens[0].trim();
            String year = tokens[1].trim();
            String director = tokens[2].trim();
            List<String> producers = new ArrayList<String>();
            for(int i = 3; i < tokens.length; ++i)
                producers.add(tokens[i].trim());
            films.add(new Film(name, year, director, producers));
        }
        return films;
    }
}
