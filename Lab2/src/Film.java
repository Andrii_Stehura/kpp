import java.util.List;

public class Film {
    private String name;
    private String director;
    private List<String> producers;
    private String year;

    public Film(String name, String year, String director, List<String> producers) {
        this.name = name;
        this.year = year;
        this.director = director;
        this.producers = producers;
    }

    public String getDirector() {
        return director;
    }

    public String getName() {
        return name;
    }

    public List<String> getProducers() {
        return producers;
    }
    public String getYear() {
        return year;
    }
}
